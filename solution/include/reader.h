#ifndef LAB3_READER_H
#define LAB3_READER_H

#include "../include/bmp_format.h"
#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>


enum read_status read_header(FILE *file_in, struct bmp_header *header);
enum read_status read_pixels(FILE *file_in, struct image *img);
size_t get_padding(struct image img);

#endif
