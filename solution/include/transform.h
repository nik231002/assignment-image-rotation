#ifndef LAB3_TRANSFORM_H
#define LAB3_TRANSFORM_H

#include "../include/image.h"

struct image rotate(struct image const source);

#endif 
