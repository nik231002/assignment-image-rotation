#include "../include/bmp_format.h"
#include "../include/reader.h"

#include <malloc.h>

static uint32_t SIGNATURE = 19778;
static uint32_t RESERVED = 0;
static uint32_t HEADER_SIZE = 40;
static uint16_t PLANES = 1;
static uint32_t COMPRESSION = 0;
static uint32_t PIXEL_PER_M = 2834;
static uint32_t COLORS_USED = 0;
static uint32_t COLORS_IMPORTANT = 0;
static size_t BIT_COUNT = 24;

static enum write_status make_header(struct image const *img, struct bmp_header *header);

enum read_status from_bmp(FILE *file_in, struct image *img) {
    if (!file_in || !img) return READ_NULL; 
    struct bmp_header header = {0};
    enum read_status header_stat = read_header(file_in, &header);
    if (header_stat) return header_stat;
    img->height = header.biHeight;
    img->width = header.biWidth;
    if (fseek(file_in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_HEADER;
    enum read_status pixel_stat = read_pixels(file_in, img);
    if (pixel_stat) { 
    	return pixel_stat; 
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *file_out, struct image *img) {
    if (((!file_out) != WRITE_OK) || ((!img)!=WRITE_OK)) return WRITE_NULL;
    struct bmp_header header = {0};
    make_header(img, &header);
    size_t a = fwrite(&header, sizeof(struct bmp_header), 1, file_out);
    if (a != 1) return WRITE_ERROR;
    if (fseek(file_out, header.bOffBits, SEEK_SET) != 0) return WRITE_NULL;
    size_t padding = get_padding(*img);
    size_t *one_line = malloc(4);
    if (!one_line) {
    	return WRITE_ERROR; 
    }
    for (size_t i = 0; i < padding; i++){
        one_line[i] = 0;
    }
    if (img->data != NULL){
        for (size_t i = 0; i < img->height; ++i) {
            size_t a = fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, file_out);
            size_t b = fwrite(one_line, padding, 1, file_out);
            if (a!=1 && b!=1){
            	free(one_line);
            	return WRITE_ERROR;
            }
        }
    }
    free(one_line);
    return WRITE_OK;
}






static enum write_status make_header(struct image const *img, struct bmp_header *header) {
    size_t padding = get_padding(*img);
    header->bfType = SIGNATURE;
    header->biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header->bfileSize = header->biSizeImage + sizeof(struct bmp_header);
    header->bfReserved = RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biXPelsPerMeter = PIXEL_PER_M;
    header->biYPelsPerMeter = PIXEL_PER_M;
    header->biClrUsed = COLORS_USED;
    header->biClrImportant = COLORS_IMPORTANT;
    return WRITE_OK;
}

