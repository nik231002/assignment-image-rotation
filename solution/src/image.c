#include "../include/image.h"
#include <malloc.h>


bool image_malloc(struct image *img){
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    return img->data == NULL;
}

void image_free(struct image *img){
    free(img->data);
}
