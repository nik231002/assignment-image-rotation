#include "../include/bmp_format.h"
#include "../include/file.h"
#include "../include/transform.h"
#include <stdbool.h>
#include <stdio.h>



int main(int argc, char **argv) {
    
    char *input_file_p = NULL;
    char *output_file_p = NULL;
    struct image img;
    
    if (argc<3) {
        fprintf(stderr, "Введено неверное число аргументов. Ожидалось 2.\n");
        return 0;
    } 
    input_file_p = argv[1];
    output_file_p = argv[2];
    
    FILE *input_file = NULL;
    FILE *output_file = NULL;
    bool in_file;
    bool out_file;
    in_file = open_file(&input_file, input_file_p, "rb");
    out_file = open_file(&output_file, output_file_p, "wb");
    if (in_file || out_file) {
        fprintf(stderr, "Произошла ошибка при попытке чтения файла.\n");
        return 0;
    }
    if (from_bmp(input_file, &img)){
        fprintf(stderr, "Произошла ошибка при попытке конвертации в image.\n");
        return 0;
    } 
    struct image rotated_img = rotate(img);
    if (to_bmp(output_file, &rotated_img)){
        fprintf(stderr, "Невозможно сконвертировать изображение в BMP\n");
        return 0;
    }

    in_file = close_file(&input_file);
    out_file = close_file(&output_file);
    if (in_file || out_file){
        fprintf(stderr, "Произошла ошибка при попытке закрытия файла\n");
        return 0;
    }
    fprintf(stdout, "Успешно!\n");
    image_free(&img);
    image_free(&rotated_img);
    return 0;
}
