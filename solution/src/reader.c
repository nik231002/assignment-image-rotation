#include "../include/reader.h"

#include <malloc.h>

enum read_status read_header(FILE *file_in, struct bmp_header *header) {
    if (fseek(file_in, 0, SEEK_END) != 0){
    	return READ_INVALID_HEADER;
    }
    if (ftell(file_in) < sizeof(struct bmp_header)){
    	return READ_INVALID_HEADER;
    }
    
    fseek(file_in, 0, SEEK_SET);
    
    
    if (fread(header, sizeof(struct bmp_header), 1, file_in) == 1){
    	return READ_OK;
    } else {
    	return READ_INVALID_HEADER;
    }
}




enum read_status read_pixels(FILE *file_in, struct image *img) {
    if (image_malloc(img)) {
    	return READ_ERROR; 
    }
    size_t padding = get_padding(*img);
    for (size_t i = 0; i < img->height; i++) {
        size_t fr = fread(img->data + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, file_in);
        if (fr != 1) return READ_NULL;
        if (fseek(file_in, padding, SEEK_CUR) != 0) return READ_NULL;
    }
    return READ_OK;
}

size_t get_padding(struct image img){
	if ((4 - img.width * 3 % 4) == 0) return 0;
	return 4 - img.width * 3 % 4;
}
