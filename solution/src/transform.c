#include "../include/transform.h"

#include <malloc.h>

struct image rotate(struct image const source){
	
	
	struct image rotated_img;
	rotated_img.data = malloc(sizeof(struct pixel) * source.height * source.width);

	for (size_t i = 0; i < source.height; i++ ) {
		for (size_t j = 0; j < source.width; j++) {
			rotated_img.data[j * source.height + (source.height - 1 - i)] = source.data[i * source.width + j];
		}
	}
	rotated_img.width = source.height;
	rotated_img.height = source.width;
	return rotated_img;
}
